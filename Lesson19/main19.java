package HomeWork.Lesson19;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class main19 {
    public static void main(String[] args) {
        String inputFile = "19products.txt";
        printOut("18out.txt", inputFile);
    }

    private static void printOut(String outFile, String inputFile) {
        try (BufferedReader reader = new BufferedReader(new FileReader(inputFile))) {

            String line;
            String breaker = "===============================================";
            int count = 0;
            float productSum;
            float totalSum = 0;
            List<String> product = new ArrayList<>();

            System.out.printf("%s %11s %8s %12s \n", "Наименование", "Цена", "Кол-во", "Стоимость");

            System.out.println(breaker);
            while ((line = reader.readLine()) != null) {
                product.add(line);
                count++;
                if (count == 3) {
                    productSum = Float.valueOf(product.get(2)) * Float.valueOf(product.get(1));
                    System.out.printf("%s %15s %s %7s %12.2f \n", product.get(0), product.get(2), "x", product.get(1), productSum);
                    totalSum += productSum;
                    count = 0;
                    product.removeAll(product);
                }
            }
            System.out.println(breaker);
            System.out.println("Total: " + totalSum);
            
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getStackTrace());
        }
    }
}
