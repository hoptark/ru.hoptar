package HomeWork.Lesson6.ScienceLab.Departments;

import HomeWork.Lesson6.ScienceLab.Research;

public class Astronomy extends Department {
    Astronomy AstrDept = new Astronomy();

    public void setAstrDept(Astronomy astrDept) {
        AstrDept.setName("Astronomy department");
        AstrDept.setEmployeesNumber(172);
    }

    public void setAstrResearches(Research[] astrResearches) {
        this.astrResearches = astrResearches;
    }
    // astronomy.setEmployeesNumber(100);    выдаёт ошибку. Пытаюсь унаследовать сеттеры и геттеры
   // astronomy.setName(Astronomy)            из родительского класса




private Research [] astrResearches;

    public Astronomy() {
        this.astrResearches = new Research[4];
        astrResearches[0] = new Research("Transneptunian objects identification", 8, 7);
        astrResearches[1] = new Research("Comet probes development", 36, 14);
        astrResearches[2] = new Research("Radiotelescoping of the vestigial radiation", 37, 19);
        astrResearches[3] = new Research("Black holes evaporation", 45, 35);

    }

    public float astrCompletion() {
        float totalDuration = 0;
        float totalCompleted = 0;
        for (Research r : astrResearches) {
            totalDuration += r.duration;
            totalCompleted += r.completed;

        }

        return totalCompleted *100 / totalDuration;

    }


}
