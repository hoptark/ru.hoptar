package HomeWork.Lesson6.ScienceLab.Departments;

import HomeWork.Lesson6.ScienceLab.Research;

public class Physics extends Department {

    Physics(int employeesNumber) {

        setEmployeesNumber(200);
    }

    private Research[] physResearches;

    public Physics() {
        this.physResearches = new Research[3];
        physResearches[0] = new Research("Gravity waves", 15, 15);
        physResearches[1] = new Research("Superconductors", 25, 2);
        physResearches[2] = new Research("Thermonuclear fusion", 151, 77);

    }

    public float physCompletion() {
        float totalDuration = 0;
        float totalCompleted = 0;
        for (Research r : physResearches) {
            totalDuration += r.duration;
            totalCompleted += r.completed;

        }

        return totalCompleted * 100 / totalDuration;

    }

        public int getResearchLength ()
        {
            return physResearches.length;
        }
    }
