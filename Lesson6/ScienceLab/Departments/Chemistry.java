package HomeWork.Lesson6.ScienceLab.Departments;

import HomeWork.Lesson6.ScienceLab.Research;

public class Chemistry extends Department {

    Chemistry(int employeesNumber) {
        setEmployeesNumber(102);
    }

    private Research[] chemResearches;

    public Chemistry() {
        this.chemResearches = new Research[2];
        chemResearches[0] = new Research("Femtochemistry", 20, 8);
        chemResearches[1] = new Research("Nanostructures synthesis", 37, 0);

    }

    public float chemCompletion() {
        float totalDuration = 0;
        float totalCompleted = 0;
        for (Research r : chemResearches) {
            totalDuration += r.duration;
            totalCompleted += r.completed;

        }

        return totalCompleted * 100 / totalDuration;

    }

}
