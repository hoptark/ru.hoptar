package HomeWork.Lesson6.ScienceLab.Departments;

import HomeWork.Lesson6.ScienceLab.Research;

public class Department {

    private Research[] res;

    public String name; // название лаборатории
    public void setName(String name) {
        this.name = name;
    }

    private int employeesNumber; //количетсво сотрудников лаборатории

    public void setEmployeesNumber(int employeesNumber) {
            this.employeesNumber = employeesNumber;
        }

    public int getEmployeesNumber() {
            return employeesNumber;
        }


}
