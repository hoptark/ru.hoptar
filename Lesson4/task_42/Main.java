package HomeWork.Lesson4.task_42;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Введите целое число для описания");
        int number = in.nextInt(); //Ввод числа для описания

        if (number != 0) {
            System.out.print((number > 0) ? "Данное число положительное и " : "Данное число отрицательное и ");
            System.out.print((number % 2 == 0) ? "чётное." : "нечётное.");

        } else {
            System.out.println("Введённое число - 0, чётное.");
        }
    }
}