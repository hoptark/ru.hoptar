package HomeWork.Lesson4.task_41;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("Введите первое число"); // Ввод первого числа для сравнения
        float number1 = in.nextFloat();

        System.out.println("Введите второе число"); // Ввод второго числа для сравнения
        float number2 = in.nextFloat();

        System.out.println("Наименьшее из чисел: " + ((number1 > number2) ? number2 : number1));

        }

}
