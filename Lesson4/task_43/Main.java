package HomeWork.Lesson4.task_43;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("Введите первое число диапазона, для которого требуется вывод таблицы умножения");
        int tableStart = in.nextInt();
        System.out.println("Введите последнее число диапазона, для которого требуется вывод таблицы умножения");
        int tableLast = in.nextInt();

        for (int number = tableStart; number <= tableLast; number++) {
            System.out.print("Таблица для " + number + ": ");
            for (int count = 1; count <= 10; count++) {
                System.out.print(number * count + " ");
            }
            System.out.println();

        }
    }

}