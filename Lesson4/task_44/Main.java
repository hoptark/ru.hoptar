package HomeWork.Lesson4.task_44;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in); // Блок ввода исходных параметров:
                                             // первого члена, шага, количества членов для вывода
        System.out.println("Please input the arithmetic sequence initial term:");
        float sequenceStart = in.nextFloat();
        System.out.println("Please input the arithmetic sequence common difference:");
        float sequenceDifference = in.nextFloat();
        System.out.println("Please input the desired number of terms to be printed out:");
        int lastTermNumber = in.nextInt();

        System.out.println("First " + lastTermNumber + " terms of the sequence are:");

        for (int termNumber = 0; termNumber < lastTermNumber; termNumber ++) { // Расчёт и вывод параметров прогрессии
            System.out.print(sequenceStart + sequenceDifference * termNumber + ", ");
            }
        System.out.println();
        // Вывод суммы всех выведенных членов
        System.out.println( "with the arithmetic series equal to " + ((2 * sequenceStart + sequenceDifference * ((lastTermNumber - 1))) * lastTermNumber/2));
    }
}
