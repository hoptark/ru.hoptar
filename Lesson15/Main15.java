package HomeWork.Lesson15;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class Main15 {

    static int a = (int) (Math.random() * 10);

    public static void main(String[] args) {

        File file = new File("Test/" + a + ".txt"); //Часть 1. Удаление рандомного файла в заданном диапазоне
        if (file.exists() == true) {
            System.out.println("file " + file.getName() + " deletion: " + file.delete());
            file.delete();
        } else {                                              //Если файла нет, создаём его
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.out.println("file creation " + e.getMessage());
            }
        }

        Path source = Paths.get("test/1.txt");          // Часть 2. Копирование...
        Path dest = Paths.get("test/" + a + "-copy.txt");

        try {
            Files.copy(source, dest);
            System.out.println(file.getName() + " copied");
        } catch (IOException e) {
            System.out.println("file copying is " + e.getMessage());
        }

        if (file.exists()) {                                // ... и переименование
            file.renameTo(new File("Test/2.txt"));
        }

        MkDir.createTestData(); // создание дерева папок в качестве тестовых данных

        dirListing(new File("test/"), 0);   // Чсть 3. Рекурсивный обход
    }

    public static void dirListing(File root, int depth) {

        //ArrayList<String> allFIles = new ArrayList<>();

        if (root.isDirectory()) {
            for (File file : root.listFiles()) {
                if (file.isDirectory()) {
                    dirListing(file, depth++);
                    System.out.println(file.getParent() + ", depth: " + depth);
                    //allFIles.add(file.getParent());

                } else {
                    System.out.println(file.getParent() + file.getName() + ", depth: " + depth);
                    //allFIles.add(file.getName());
                }
            }
        }
        //System.out.println("total list: " + allFIles);
    }
}
