package HomeWork.Lesson15;

import java.io.File;

public class MkDir {
    public static void createTestData() {
        try {
            new File("test/test1/a/a").mkdirs();
            new File ("test/test1/a/b").mkdirs();
            new File ("test/test1/b/a").mkdirs();
            new File ("test/test1/b/b").mkdirs();
            new File ("test/test1/b/c").mkdirs();
            new File ("test/test1/b/c/d").mkdirs();
            new File ("test/test1/a/b/c/d/e").mkdirs();

            new File ("test/test1/a/a/a.txt").createNewFile();
            new File ("test/test1/a/b/b.txt").createNewFile();
            new File ("test/test1/b/a/ba.txt").createNewFile();
            new File ("test/test1/b/b/bb.txt").createNewFile();
            new File ("test/test1/b/c/bc.txt").createNewFile();
            new File ("test/test1/b/c/d/bcd.txt").createNewFile();
            new File ("test/test1/a/b/c/d/e/abcd.txt").createNewFile();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
