package HomeWork.Lesson17;
import java.io.Serializable;
import java.util.Scanner;

public class Book implements Serializable {
    final static long serialVersionUID = 11;
    String title, author;
    int year;
    Scanner scanner = new Scanner(System.in);

    public Book(String title, String author, int year) {
        this.title = scanner.nextLine();
        this.author = scanner.nextLine();
        this.year = scanner.nextInt();
    }

//    public void bookSet() { // введение атрибутов книги
//        this.title = scanner.nextLine();
//        this.author = scanner.nextLine();
//        this.year = scanner.nextInt();
//    }

    public String getTitle() { return title; }

    public String getAuthor() {
        return author;
    }

    public int getYear() {
        return year;
    }

//    public Book getBook () {
//        Book book = new Book;
//        this author = book.getAuthor();
//    }
}

