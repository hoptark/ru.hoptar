package HomeWork.Lesson17;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Scanner;

public class ScannerExample {
    public static void main(String[] args) {
        //InputStream in = System.in;
        try (InputStream is = new FileInputStream("17library.txt")) {
            printStream(is);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static void printStream(InputStream in) {
        try (Scanner sc = new Scanner(in)) {
            while (sc.hasNext()) {
                String s = sc.next();
                System.out.print(s);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}