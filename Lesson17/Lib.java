package HomeWork.Lesson17;

import java.io.*;

public class Lib {

    final static String fileName = "17Library.txt";

    public void LibAdd() {
        try (
                FileOutputStream fos = new FileOutputStream(fileName);
                ObjectOutputStream oos = new ObjectOutputStream(fos)
                ) {
            Book book = new Book("title", "author", 0);

            oos.writeObject(book);
            oos.close();
            fos.close();
            System.out.println("adding");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getCause());
        }
    }

    public Book LibRead() {
            Book book = null;
        try (
                FileInputStream fis = new FileInputStream(fileName);
                ObjectInputStream ois = new ObjectInputStream(fis)
        ) {
            boolean emptyCheck = fileName.isEmpty();
            System.out.println(emptyCheck);
            book = (Book) ois.readObject();

        } catch (ClassNotFoundException e) {
            System.out.println("class not found");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("file not found");
        } catch (IOException e) {
            System.out.println((e.getMessage()));
            System.out.println("file is busy");
        } catch (NullPointerException e) {
            e.getMessage();
            e.getStackTrace();
            e.getCause();
        }
        return book;
    }
}
