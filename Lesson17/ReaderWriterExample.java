package HomeWork.Lesson17;

import java.io.*;

public class ReaderWriterExample {
    public static void main(String[] args) {
//        try(OutputStream os = new FileOutputStream("17file.txt");
//        PrintStream ps = new PrintStream(os)) {
//            ps.print("Hello!");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        try (InputStream is = new FileInputStream("17file.txt");
        InputStreamReader isr = new InputStreamReader(is)) {
            char[] buf = new char[100];
         //   int len = 1;
            while (isr.read(buf) > 0) {
                System.out.println(new String(buf));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
