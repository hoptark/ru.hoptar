package HomeWork.Lesson17;

import java.io.*;

public class CopyStreamExample {
    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        try (
          InputStream fis = new FileInputStream("17somefile.bin");
          OutputStream fos = new FileOutputStream("17copy.bin");
        ) {
//            int b = fis.read();
//            while(b >= 0) {
//                fos.write(b);
//                b = fis.read();

            byte[] buffer = new byte[10000];
            int len;
            while ((len = fis.read(buffer)) > 0) {
                fos.write(buffer, 0, len);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        System.out.println(System.currentTimeMillis() - start);
    }
}

