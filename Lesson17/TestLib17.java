package HomeWork.Lesson17;

import java.io.FileOutputStream;
import java.io.IOException;

public class TestLib17 {
    public static void main(String[] args) {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream("17somefile. bin");
            for (byte b = 32; b < 127; b++) {
                fos.write(b);
            }
            fos.write("\n".getBytes());
            fos.write("Hello, world!".getBytes());
        } catch (IOException e) {
            System.out.println(e.getMessage());

        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

