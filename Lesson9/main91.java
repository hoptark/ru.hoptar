package HomeWork.Lesson9;

import HomeWork.Lesson9.Animals.*;
import HomeWork.Lesson9.Animals.Human.*;

public class main91 {


    public static void main(String[] args) {

        Wolf wolf = new Wolf("wolf");

        wolf.getName();
        System.out.println(wolf.getName());
        wolf.Runs();
        wolf.Swims();

        Duck duck = new Duck("duck");
        System.out.println(duck.getName());
        duck.Flies();
        duck.Runs();
        duck.Swims();

        Fish fish = new Fish("fish");
        System.out.println(fish.getName());
        fish.Swims();

        Runner runner = new Runner ("runner", 5,10);
        runner.Runs();
        runner.Swims();

        Swimmer swimmer = new Swimmer ("swimmer", 1, 0);
        swimmer.Swims();
        swimmer.Runs();

    }
}
