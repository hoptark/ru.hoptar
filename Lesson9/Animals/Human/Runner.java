package HomeWork.Lesson9.Animals.Human;

public class Runner extends Human implements Human.Runnable, Human.Swimmable {

    int runSpeed;

    public Runner (String name, float position, int runSpeed) {
        super(name, position);
        this.runSpeed = runSpeed;
    }

    @Override
    void Shifts() {
        this.position = (int) (Math.random() * runSpeed);
            }

    @Override
    public void Swims() {
        System.out.println("a runner cannot swim well so he never tries to");

    }

    @Override
    public void Runs() {
        this.Shifts();
        System.out.println("Curretnt runner's position is " + this.position + "; current speed is " + this.runSpeed);
        if (runSpeed == 0) {
            System.out.println("Sorry, I'm disabled");
       } else if (runSpeed < 5) {
            System.out.println("I'm trash at running");
        } else {
            System.out.println("I'm a blazing fast runner");
        }
    }
}
