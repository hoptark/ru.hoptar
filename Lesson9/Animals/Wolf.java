package HomeWork.Lesson9.Animals;

    public class Wolf extends Animal implements Animal.Runnable, Animal.Swimmable {

        public Wolf (String name) {
            super(name);
        }

        @Override
    public void Runs() {
        System.out.println("Wolf runs");
    }

    @Override
    public void Swims() {
        System.out.println("Wolf swims");
    }
}
