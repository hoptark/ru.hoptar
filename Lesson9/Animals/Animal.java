package HomeWork.Lesson9.Animals;

public abstract class Animal {

    public String name;

    public Animal(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public interface Runnable {
        void Runs();
    }

    public interface Flyable {
        void Flies();
    }

    public interface Swimmable {
        void Swims();
    }

}
