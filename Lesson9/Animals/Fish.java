package HomeWork.Lesson9.Animals;

public class Fish extends Animal implements Animal.Swimmable {

    public Fish (String name) {
        super(name);
    }

    @Override
    public void Swims() {
        System.out.println("Fish can swim \n");
    }
}
