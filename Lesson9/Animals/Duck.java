package HomeWork.Lesson9.Animals;

public class Duck extends Animal implements Animal.Runnable, Animal.Swimmable, Animal.Flyable {


    public Duck (String name) {
        super(name);
        }


    @Override
    public void Runs() {
        System.out.println("Duck can run");
    }

    @Override
    public void Flies() {
        System.out.println("Duck can fly");
    }

    @Override
    public void Swims() {
        System.out.println("Duck can swim");
    }
}


