package HomeWork.Lesson3.task_34;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        int random_number = (int) (Math.random() * 100);
        int number = 0;
        System.out.println("Введите целое число от 0 до 100; для выхода введите отрицательное число \n");

        while (true) {
            Scanner in = new Scanner(System.in);
            int number1 = in.nextInt();
            if (number1 < 0) {
                System.out.println("Всего доброго!");
                break;
            } else {
                if (number1 == random_number) {
                    System.out.println("Поздравляю, вы угадали! Загаданное число - " + random_number);
                    break;
                } else {
                    if (Math.abs(random_number - number) >= Math.abs(random_number - number1)) {
                        System.out.println("Горячо! \n");
                    } else {
                        System.out.println("Холодно! \n");
                    }

                }
            }
            number = number1;
        }

    }
}


