package HomeWork.Lesson3.task_32;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.print("please enter your gross salary \n");
        int salary = in.nextInt();

        System.out.print("please enter your country's profit tax rate, % \n");
        int taxRate = in.nextInt();

        System.out.println("your earning after tax is " + (salary - salary * taxRate / 100 + " rubles"));
    }

}
