package HomeWork.Lesson14;

public enum Drink {
    KVAS("Квас", 30, 20 ),
    COLA ("Кола", 50, 20),
    TEA("Чай", 20, 20),
    BLACKCOFFEE("Чёрный кофе", 45, 20),
    WHITECOFFEE("Кофе с молоком", 55, 20),
    HOTCHOCKOLADE("Горячий шоколад", 60, 20);

    String title;   //название напитка
    int price;      //цена
    int drinkStock; //количество отсавшихся порций/банок

    Drink(String title, int price, int drinkStock) {
        this.title = title;
        this.price = price;
        this.drinkStock = (int) (Math.random()*7);
    }

}
