package HomeWork.Lesson14;
import java.io.FileInputStream;
import java.util.logging.*;
import java.util.Scanner;

public class Main14 {

    final static Logger logger = Logger.getLogger(Main14.class.getName());

    public static void main(String[] args) throws Exception {

        LogManager.getLogManager().readConfiguration(new FileInputStream("logging.properties"));
        logger.info("System started, all running smooth");

        Scanner scanner = new Scanner(System.in);

        int chosenPrice = 0;    // цена выбранного напитка
        int cashPaid;           // уплаченная сумма
        int drinksLeft = 0;     // остаток данного вида продукта
        String drink;           // выбранный напиток
        String choice;          // выбор пользователя

        System.out.println("Добрый день! Выберите напиток или введите Меню для вывода перечня напитков.");

        while (true) {

            drink = null;
            choice = scanner.nextLine();

            if (choice.equals("Меню") || choice.equals("меню")) { //вывод меню напитков
                for (Drink d : Drink.values()) {
                    System.out.println(d.title + ", " + d.price + ". Доступно - " + d.drinkStock);
                }
                continue;
            }

            for (Drink d : Drink.values()) {

                if (choice.equals(d.title) && d.drinkStock > 0) { //введено верное название, напиток доступен
                    drink = d.title;
                    chosenPrice = d.price;
                    d.drinkStock -= 1;
                    drinksLeft = d.drinkStock;
                    String warningMessage = d.drinkStock > 5 ? "Stock is good" : "Low " + d.name() + " stock level!";
                    logger.warning(warningMessage); //предупреждение об уровне остатка
                    break;

                } else if (choice.equals(d.title) && d.drinkStock == 0) { //название введено верно, но напитка нет
                    drink = "out";
                    logger.severe(String.format("Out of %s drinks", d.name())); //сообщение об отсутствии напитка
                    System.out.printf("Извините, напиток %s закончился. Пожалуйста, сделайте другой выбор \n", d.title);
                    break;
                }
            }
            if (drink != null && !drink.equals("out")) {
                break;
            } else if (drink == null){
                    System.out.println("Введено неверное название. Пожалуйста, введите снова");
                    logger.info("Wrong drink type entered");
                }
            }

        System.out.println("Вы выбрали " + drink + ". Пожалуйста, внесите " + chosenPrice + "р.");

        for (cashPaid = 0; cashPaid < chosenPrice; cashPaid += scanner.nextInt()) {
            System.out.println("Внесено " + cashPaid + "р.; Осталось внести: " + (chosenPrice - cashPaid) + "р.");
        }

        logger.info(String.format("There are %d %s left", drinksLeft, drink)); //информационное сообщение
        System.out.println("Внесено " + cashPaid + "р. Пожалуйста, подождите.\n" + "Напиток " + drink +
                " готов. Пожалуйста, не забудьте сдачу: " + (cashPaid - chosenPrice) + "р.");
        logger.fine("The transaction has been successfully completed"); //подтверждение удачной покупки
    }
}
