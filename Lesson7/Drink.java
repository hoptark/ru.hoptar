package HomeWork.Lesson7;

public enum Drink {
    KVAS("Квас", 30 ),
    COLA ("Кола", 50),
    TEA("Чай", 20),
    BLACKCOFFEE("Чёрный кофе", 45),
    WHITECOFFEE("Кофе с молоком", 55),
    HOTCHOCKOLADE("Горячий шоколад", 60);

    String title;
    int price;

    Drink(String title, int price) {
        this.title = title;
        this.price = price;
    }

}
