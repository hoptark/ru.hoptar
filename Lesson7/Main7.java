package HomeWork.Lesson7;


import java.util.Scanner;

public class Main7 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int chosenPrice = 0; // цена выбранного напитка
        int cashPaid;    // уплаченная сумма
        String drink = null; // выбранный напиток
        String choice;       // выбор пользователя

        System.out.println("Добрый день! Выберите напиток или введите Меню для вывода перечня напитков.");

        while (true) {

            choice = scanner.nextLine();
            for (Drink d : Drink.values()) {
                if (choice.equals(d.title)) {
                    drink = d.title;        // собственно, switch уже и не нужен
                    chosenPrice = d.price;
                }
            }

            if (choice.equals("Меню") || choice.equals("меню")) { //вывод меню напитков
                for (Drink d : Drink.values()) {
                    System.out.println(d.title + ", " + d.price + " р.");
                }

            } else if (choice.equals(drink)) {
                break;
            } else {
                System.out.println("Введено неверное название, пожалуйста, введите снова");
            }
        }


        System.out.println("Вы выбрали " + drink + ". Пожалуйста, внесите " + chosenPrice + "р.");

        for (cashPaid = 0; cashPaid < chosenPrice; cashPaid += scanner.nextInt()) {
            System.out.println("Внесено " + cashPaid + "р.; Осталось внести: " + (chosenPrice - cashPaid) + "р.");
        }

        System.out.println  ("Внесено " + cashPaid + "р. Пожалуйста, подождите.\n"  + "Напиток " + drink +
                            " готов. Пожалуйста, не забудьте сдачу: " + (cashPaid - chosenPrice) + "р.");
    }
}



// Первое решение. Пока копался с оптимизацией, понял, как обойтись без switch, но для себя решил оставить.
//            if (choice.equals("Меню") || choice.equals("меню")) { //вывод меню напитков
//                for (Drink d : Drink.values()) {
//                    System.out.println(d.title + ", " + d.price + " р.");
//                }
//            } else {
//                if (choice.equals(COLA.title) || choice.equals(KVAS.title) || choice.equals(TEA.title) ||
//                        choice.equals(BLACKCOFFEE.title) || choice.equals(WHITECOFFEE.title) ||
//                        choice.equals(HOTCHOCKOLADE.title)) {
//                   // for (Drink d : Drink.values()) {if(choice.equals.(d.title)) { - так не работало, потребовалось
//                              разделение на присваивание в цикле и отдельную проверку вне его тела;
//
//                    break;
//                } else {
//                    System.out.println("Введено неверное название, пожалуйста, введите снова");
//                }
//            }
//        }

//        switch (choice) {
//
//            case "Кола":
//                chosenPrice = COLA.price; drink = COLA.title; break;
//
//            case "Квас":
//                chosenPrice = KVAS.price; drink = KVAS.title; break;
//
//            case "Чай":
//                chosenPrice = TEA.price; drink = TEA.title; break;
//
//            case "Чёрный кофе":
//                chosenPrice = BLACKCOFFEE.price; drink = BLACKCOFFEE.title; break;
//
//            case "Кофе с молоком":
//                chosenPrice = WHITECOFFEE.price; drink = WHITECOFFEE.title; break;
//
//            case "Горячий шоколад":
//                chosenPrice = HOTCHOCKOLADE.price; drink = HOTCHOCKOLADE.title; break;
//        }


//        while (cashPaid < chosenPrice) {
//            cashPaid = cashPaid + scanner.nextInt();
//            System.out.println("Внесено " + cashPaid + "р.; Осталось внести: " + (chosenPrice - cashPaid));
//        }


//
//        String choice = scanner.nextLine();
//
//        System.out.println("Выберите напиток или введите Меню для вывода перечня напитков");
//
//
//        if (choice == "Меню") {
//            System.out.println(Arrays.toString(Drink.values()));
//        }   else {
//                switch (choice) {
//                    case "Квас":
//                        System.out.println();
//                }
//        }
//    }