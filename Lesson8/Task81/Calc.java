package HomeWork.Lesson8.Task81;

import java.util.Scanner;

import static HomeWork.Lesson8.Task81.CalcApp.result;

public class Calc {

    static Scanner input = new Scanner(System.in);

    static void Add() {
        float inputNumber = input.nextFloat();
        result += inputNumber;
        }

    static void Subtract() {
        float inputNumber = input.nextFloat();
        result -= inputNumber;
        }

    static void Multiply() {
        float inputNumber = input.nextFloat();
        result *= inputNumber;
    }

     static void Divide() {
         float inputNumber = input.nextFloat();
            result = result / inputNumber;
    }

    static void PerCent() {
        float inputNumber = input.nextFloat();
        result = (result * inputNumber) / 100;
    }
}