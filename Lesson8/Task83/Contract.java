package HomeWork.Lesson8.Task83;

public class Contract {

    String date;
    int number;
    String[] goodsList;

    public Contract(String date, int number) {
        this.date = date;
        this.number = number;
    }

    public String getDate() {
        return date;
    }

    public int getNumber() {
        return number;
    }

    public String[] getGoodsList() {
        return goodsList;
    }
}
