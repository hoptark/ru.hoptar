package HomeWork.Lesson8.Task82;

import java.util.ArrayList;
import java.util.List;

public class Main82 {

    static int cookiesTypeCount = 0;                    // счётчик разных видов печенек
    static int cookiessNumber = 0;                      // счётчик общего количества печенек всех видов
    static List<String> Bisc = new ArrayList<>();       // хранилище наименований изделий
    static List<Integer> cookiesN = new ArrayList<>();  // хранилище соответствующего количества изделий

    static Cookies cookies = new Cookies(" ", 0);

    public static void Baking() {
        cookies.setNumber(1);
        cookies.setName(cookies.getName());
    }

    public static void main(String[] args) {

        System.out.println("Добрый день! У нас есть для вас печеньки ВСЕХ видов! Какие желаете?" +
                " (чтобы закончить выбор, введите 'хватит')");

        cookies.name = "start";

        while (!cookies.name.equals("хватит")) {

            Baking();

            if (cookies.getName().equals("хватит")||cookies.getName().equals("Хватит")) {
                break;
            } else {
                System.out.println("Мы испекли для вас " + cookies.getNumber() + " печенек " + cookies.getName() +
                        ". Ещё печенек?");
                Bisc.add(cookies.getName());
                cookiesN.add(cookies.getNumber());
                cookiesTypeCount += 1;
                cookiessNumber += cookies.getNumber();
            }
        }
        System.out.println("Отлично! Ваш заказ:" + cookiesTypeCount + " видов печенек, общим количеством " + cookiessNumber + " шт.:");
        for (int b = 0; b < Bisc.size(); b++) {
            System.out.println(Bisc.get(b) + " " + cookiesN.get(b));
        }
        System.out.println("Спасибо за заказ!");
    }
}