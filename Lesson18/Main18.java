package HomeWork.Lesson18;

import java.io.*;
import java.nio.charset.Charset;

public class Main18 {
    public static void main(String[] args) {
        String inputFile = "18source.txt";
        changeEncoding("18out.txt", inputFile, "koi8");
    }

    private static void changeEncoding(String outFile, String inputFile, String charsetName) {
        try (FileInputStream fis = new FileInputStream(inputFile);
            FileOutputStream fos = new FileOutputStream(outFile)) {
            byte[] buffer = new byte[10000];
            String s = "";
            while ((fis.read(buffer)) > 0) {
                s += new String(buffer);
            }
            fos.write(s.getBytes(Charset.forName(charsetName)));

        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getStackTrace());
        }
    }
}