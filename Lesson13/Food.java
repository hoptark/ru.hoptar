package HomeWork.Lesson13;

public enum Food {
    Porridge("каша"),
    Fruits("фрукты"),
    Soup("суп"),
    Sweets("конфеты"),
    Vegetables("овощи");

    String name;
    Food (String name) {
        this.name = name;
    }
}
