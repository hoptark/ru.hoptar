package HomeWork.Lesson13;

public class Kid {
    public void Eat() throws Exception {

        while (true) {
            int i = (int) (Math.random() * 5);
            for (Food f : Food.values()) {
                if (i == f.ordinal()) {
                    if (f.equals(Food.Vegetables)) {
                        System.out.println("Эээ... Спасибо, мама, отличные " + Food.Vegetables.name + ", но я уже сыт");
                        throw new Exception();
                    } else {
                        System.out.println("Спасибо, мама, " + f.name + " - отличная еда, обожаю!");
                    }
                }
            }
        }
    }
}